import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @see <a href="https://www.youtube.com/watch?v=DL_ZMWru1lU">Sistema de
 *      comunicació de Jason Becker</a>
 * @version 1
 * @author Ezequiel Simón
 * @since 20-2-20
 *
 *
 */
public class LaVeuDeJason {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		/**
		 * Variable entera i per comptar el número de tokens
		 */
		int i = 0;

		/**
		 * Variable que recull les instruccions de l'usuari
		 */
		String entrada = "  ";
		/**
		 * Variable que guarda les diferents lletres formant paraules
		 */
		String sortida = "";

		do {
			/**
			 * Vector que guarda les diferents instruccions tipus AM (amunt), BA (baix), ES
			 * (esquerra), DR (dreta).
			 */
			String[] ordres = { "  ", "  ", "  ", "  " };
			entrada = reader.nextLine().toUpperCase();
			StringTokenizer token = new StringTokenizer(entrada);

			while (token.hasMoreTokens()) {
				ordres[i] = token.nextToken();
				i++;
			}
			i = 0;
			sortida = parlar(ordres, sortida);

		} while (!entrada.contentEquals("."));
		System.out.println(sortida);
		reader.close();
	}

	/**
	 * 
	 * @param ordres  Vector amb les direccions guardades.
	 * @param sortida String on s'emmagatzemen les diferents lletres formant una
	 *                paraula.
	 * @return Retorna un String amb les diferents lletres concatenades.
	 */
	static String parlar(String[] ordres, String sortida) {
		if (ordres[0].contentEquals("AM")) {

			switch (ordres[1]) {
			case "ES":

				if (ordres[2].contentEquals("AM")) {
					sortida = sortida + "B";
				} else if (ordres[2].contentEquals("BA")) {
					sortida = sortida + "D";
				} else if (ordres[2].contentEquals("ES")) {
					sortida = sortida + "A";
				} else if (ordres[2].contentEquals("DR")) {
					sortida = sortida + "C";
				}
				break;
			case "AM":

				if (ordres[2].contentEquals("AM")) {
					sortida = sortida + "F";
				} else if (ordres[2].contentEquals("BA")) {
					sortida = sortida + "H";
				} else if (ordres[2].contentEquals("ES")) {
					sortida = sortida + "E";
				} else if (ordres[2].contentEquals("DR")) {
					sortida = sortida + "G";
				}
				break;
			case "DR":

				if (ordres[2].contentEquals("AM")) {
					sortida = sortida + "J";
				} else if (ordres[2].contentEquals("BA")) {
					sortida = sortida + "L";
				} else if (ordres[2].contentEquals("ES")) {
					sortida = sortida + "I";
				} else if (ordres[2].contentEquals("DR")) {
					sortida = sortida + "K";
				}
				break;
			}

		} else if (ordres[0].contentEquals("BA")) {

			switch (ordres[1]) {
			case "ES":

				if (ordres[2].contentEquals("AM")) {
					sortida = sortida + "N";
				} else if (ordres[2].contentEquals("BA")) {
					sortida = sortida + "P";
				} else if (ordres[2].contentEquals("ES")) {
					sortida = sortida + "M";
				} else if (ordres[2].contentEquals("DR")) {
					sortida = sortida + "O";
				}
				break;
			case "BA":

				if (ordres[2].contentEquals("AM")) {
					sortida = sortida + "R";
				} else if (ordres[2].contentEquals("BA")) {
					sortida = sortida + "T";
				} else if (ordres[2].contentEquals("ES")) {
					sortida = sortida + "Q";
				} else if (ordres[2].contentEquals("DR")) {
					sortida = sortida + "S";
				}
				break;
			case "DR":

				if (ordres[2].contentEquals("AM")) {
					sortida = sortida + "V";
				} else if (ordres[2].contentEquals("BA")) {

					if (ordres[3].contentEquals("ES")) {
						sortida = sortida + "Y";
					} else if (ordres[3].contentEquals("DR")) {
						sortida = sortida + "Z";
					} else {
						sortida = sortida + "X";
					}

				} else if (ordres[2].contentEquals("ES")) {
					sortida = sortida + "U";
				} else if (ordres[2].contentEquals("DR")) {
					sortida = sortida + "W";
				}
				break;
			}

		}
		return sortida;
	}

}
