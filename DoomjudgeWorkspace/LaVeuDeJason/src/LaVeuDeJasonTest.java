import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LaVeuDeJasonTest {

	@Test
	void testParlar() {
		String[] h = {"AM","AM","BA"};
		String[] o = {"BA","ES","DR"};
		String[] l = {"AM","DR","BA"};
		String[] a = {"AM","ES","ES"};
		
		String pipo="";
		String sortida1 =LaVeuDeJason.parlar(h, pipo);
		//Comprovació  paraula
		sortida1 = sortida1 + LaVeuDeJason.parlar(o, pipo);
		sortida1 = sortida1 + LaVeuDeJason.parlar(l, pipo);
		sortida1 = sortida1 + LaVeuDeJason.parlar(a, pipo);
		assertEquals("HOLA",sortida1);
		
		pipo="";
		
		//Comprobació matriu 1
		sortida1 =LaVeuDeJason.parlar(a, pipo);
		assertEquals("A",sortida1);
		String[] b = {"AM","ES","AM"};
		sortida1 =LaVeuDeJason.parlar(b, pipo);
		assertEquals("B",sortida1);
		String[] c = {"AM","ES","DR"};
		sortida1 =LaVeuDeJason.parlar(c, pipo);
		assertEquals("C",sortida1);
		String[] d = {"AM","ES","BA"};
		sortida1 =LaVeuDeJason.parlar(d, pipo);
		assertEquals("D",sortida1);
		
		//Comprobació matriu 2
		sortida1 =LaVeuDeJason.parlar(h, pipo);
		assertEquals("H",sortida1);
		String[] e = {"AM","AM","ES"};
		sortida1 =LaVeuDeJason.parlar(e, pipo);
		assertEquals("E",sortida1);
		String[] g = {"AM","AM","DR"};
		sortida1 =LaVeuDeJason.parlar(g, pipo);
		assertEquals("G",sortida1);
		String[] f = {"AM","AM","AM"};
		sortida1 =LaVeuDeJason.parlar(f, pipo);
		assertEquals("F",sortida1);
		
		//Comprobació matriu 3
		sortida1 =LaVeuDeJason.parlar(l, pipo);
		assertEquals("L",sortida1);
		String[] i = {"AM","DR","ES"};
		sortida1 =LaVeuDeJason.parlar(i, pipo);
		assertEquals("I",sortida1);
		String[] k = {"AM","DR","DR"};
		sortida1 =LaVeuDeJason.parlar(k, pipo);
		assertEquals("K",sortida1);
		String[] j = {"AM","DR","AM"};
		sortida1 =LaVeuDeJason.parlar(j, pipo);
		assertEquals("J",sortida1);
		
		//Comprobació matriu 4
		sortida1 =LaVeuDeJason.parlar(o, pipo);
		assertEquals("O",sortida1);
		String[] p = {"BA","ES","BA"};
		sortida1 =LaVeuDeJason.parlar(p, pipo);
		assertEquals("P",sortida1);
		String[] m = {"BA","ES","ES"};
		sortida1 =LaVeuDeJason.parlar(m, pipo);
		assertEquals("M",sortida1);
		String[] n = {"BA","ES","AM"};
		sortida1 =LaVeuDeJason.parlar(n, pipo);
		assertEquals("N",sortida1);
		
		//Comprobació matriu 5
		String[] t = {"BA","BA","BA"};
		sortida1 =LaVeuDeJason.parlar(t, pipo);
		assertEquals("T",sortida1);
		String[] q = {"BA","BA","ES"};
		sortida1 =LaVeuDeJason.parlar(q, pipo);
		assertEquals("Q",sortida1);
		String[] s = {"BA","BA","DR"};
		sortida1 =LaVeuDeJason.parlar(s, pipo);
		assertEquals("S",sortida1);
		String[] r = {"BA","BA","AM"};
		sortida1 =LaVeuDeJason.parlar(r, pipo);
		assertEquals("R",sortida1);
		
		//Comprobació matriu 6
		String[] v = {"BA","DR","AM"};
		sortida1 =LaVeuDeJason.parlar(v, pipo);
		assertEquals("V",sortida1);
		String[] u = {"BA","DR","ES"};
		sortida1 =LaVeuDeJason.parlar(u, pipo);
		assertEquals("U",sortida1);
		String[] w = {"BA","DR","DR"};
		sortida1 =LaVeuDeJason.parlar(w, pipo);
		assertEquals("W",sortida1);
		String[] x = {"BA","DR","BA"," "};
		sortida1 =LaVeuDeJason.parlar(x, pipo);
		assertEquals("X",sortida1);
		String[] y = {"BA","DR","BA","ES"};
		sortida1 =LaVeuDeJason.parlar(y, pipo);
		assertEquals("Y",sortida1);
		String[] z = {"BA","DR","BA","DR"};
		sortida1 =LaVeuDeJason.parlar(z, pipo);
		assertEquals("Z",sortida1);
		
	}

}
